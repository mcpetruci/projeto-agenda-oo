package luis_lista_final;

import java.util.Scanner;

public class Ex1 {

    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);

        double[] n = new double[150];
        double t = 0;

        for (int i = 0; i < n.length; i++) {

            System.out.println("Digite a nota do aluno " + i);

            n[i] = ler.nextDouble();

            t = t + n[i];

        }

        System.out.println("A média geral é: " + t / 150);

    }

}
