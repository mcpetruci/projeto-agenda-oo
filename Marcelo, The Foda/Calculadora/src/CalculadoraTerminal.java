
import java.util.Scanner;

public class CalculadoraTerminal {

    public static double somar(double a, double b) {
        return a + b;
    }

    public static double subtrair(double a, double b) {
        return a - b;
    }

    public static double multiplicacao(double a, double b) {
        return a * b;
    }

    public static double divisao(double a, double b) {
        return a / b;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double numero1;
        double numero2;
        double resultado;
        int opcao;

        String menu = "############# Calculadora #############\n"
                + "1 - Soma\n"
                + "2 - Subtração\n"
                + "3 - Multiplicação\n"
                + "4 - Divisão\n"
                + "0 - Sair\n"
                + "Entre com a opção";
        do {
            System.out.println(menu);

            opcao = scanner.nextInt();

            switch (opcao) {
                case 1:
                    System.out.println("Digite valor:");
                    numero1 = scanner.nextDouble();
                    System.out.println("Digite valor:");
                    numero2 = scanner.nextDouble();
                    resultado = somar(numero1, numero2);
                    System.out.println("Resultado:" + resultado);
                    break;

                case 2:
                    System.out.println("Digite valor:");
                    numero1 = scanner.nextDouble();
                    System.out.println("Digite valor:");
                    numero2 = scanner.nextDouble();
                    resultado = subtrair(numero1, numero2);
                    System.out.println("Resultado:" + resultado);
                    break;

                case 3:
                    System.out.println("Digite valor:");
                    numero1 = scanner.nextDouble();
                    System.out.println("Digite valor:");
                    numero2 = scanner.nextDouble();
                    resultado = multiplicacao(numero1, numero2);
                    System.out.println("Resultado:" + resultado);
                    break;

                case 4:
                    System.out.println("Digite valor:");
                    numero1 = scanner.nextDouble();
                    System.out.println("Digite valor:");
                    numero2 = scanner.nextDouble();
                    resultado = divisao(numero1, numero2);
                    System.out.println("Resultado:" + resultado);
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:

                    System.out.println("Opcao invalida!!!!!");

            }
        } while (true);

    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
