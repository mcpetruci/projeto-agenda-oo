
import javax.swing.JOptionPane;

public class Calculadora {

    public static double somar(double a, double b) {
        return a + b;
    }

    public static double subtrair(double a, double b) {
        return a - b;
    }

    public static double multiplicacao(double a, double b) {
        return a * b;
    }

    public static double divisao(double a, double b) {
        return a / b;
    }

    public static double lerValor(String mensagem) {
        String aux;
        aux = JOptionPane.showInputDialog(mensagem);
        return Double.parseDouble(aux);
    }

    public static void exibirResultado(double r) {
        JOptionPane.showMessageDialog(null, "Resultado: " + r);
    }

    public static void main(String[] args) {

        double numero1;
        double numero2;
        double resultado;
        int opcao;
        String aux;

        String menu = "############# Calculadora #############\n"
                + "1 - Soma\n"
                + "2 - Subtração\n"
                + "3 - Multiplicação\n"
                + "4 - Divisão\n"
                + "0 - Sair\n"
                + "Entre com a opção";
        do {
            aux = JOptionPane.showInputDialog(menu);
            opcao = Integer.parseInt(aux);

            switch (opcao) {
                case 1:
                    
                    
                    
                    numero1 = lerValor("Digite numero 1");
                    numero2 = lerValor("Digite numero 2");
                    resultado = somar(numero1, numero2);
                    exibirResultado(resultado);

                    break;
                case 2:
                    numero1 = lerValor("Digite numero 1");
                    numero2 = lerValor("Digite numero 2");
                    resultado = subtrair(numero1, numero2);
                    exibirResultado(resultado);
                    break;
                case 3:
                    numero1 = lerValor("Digite numero 1");
                    numero2 = lerValor("Digite numero 2");
                    resultado = multiplicacao(numero1, numero2);
                    exibirResultado(resultado);
                    break;
                case 4:
                    numero1 = lerValor("Digite o denominador");
                    numero2 = lerValor("Digite Quociente");
                    resultado = divisao(numero1, numero2);
                    exibirResultado(resultado);
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    showErrorMessage("Opção Inválida!!!!");

            }

        } while (true);

    }

    public static void showErrorMessage(String m) {
        JOptionPane.showMessageDialog(null, m, "Erro", JOptionPane.ERROR_MESSAGE);

    }

}
